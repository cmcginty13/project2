package gordon.colin.mobile.adventure;

import java.util.Set;

public interface Location {
	
	public void addNeighbor(Location location);
	
	public Location[] getNeighborsArray();
	
	public Set<Location> getNeighborsSet();
	
	public boolean actorEnter(Actor actor);
	
	public boolean actorExit(Actor actor);
	
	public Actor[] getActors();
}
