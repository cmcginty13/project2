package gordon.colin.mobile.adventure;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class DrawingThread extends Thread {
	private DrawTest sview;
	private SurfaceHolder sh;
	private Canvas canvas;
	private Location terrain;

	public DrawingThread(SurfaceHolder surHolder, DrawTest drawTest, Location t,
			MotionEvent event) {
		sview = drawTest;
		sh = surHolder;
		terrain = t;
	}

	public DrawingThread(SurfaceHolder surHolder, DrawTest drawTest, Location t) {
		sview = drawTest;
		sh = surHolder;
		terrain = t;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			canvas = sh.lockCanvas();

			canvas.drawRGB((int) (Math.random() * 256),
					(int) (Math.random() * 256), (int) (Math.random() * 256));

			// draw terrain here

			sh.unlockCanvasAndPost(canvas);
		}
	}
}