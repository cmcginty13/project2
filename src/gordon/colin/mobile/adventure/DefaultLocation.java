package gordon.colin.mobile.adventure;

import java.util.HashSet;
import java.util.Set;

public class DefaultLocation implements Location {

	private Set<Location> neighbors;
	private Set<Actor> actors;
	private int x, y; // used to indicate where this location is on the map,
						// with (0,0) being the top left corner

	public DefaultLocation() {
		neighbors = new HashSet<Location>();
		actors = new HashSet<Actor>();
	}

	public DefaultLocation(int x, int y) {
		this();
		this.x = x;
		this.y = y;
	}

	@Override
	public void addNeighbor(Location location) {
		neighbors.add(location);
	}

	@Override
	public Location[] getNeighborsArray() {
		return (Location[]) neighbors.toArray();
	}

	@Override
	public boolean actorEnter(Actor actor) {
		return actors.add(actor);
	}

	@Override
	public boolean actorExit(Actor actor) {
		return actors.remove(actor);
	}

	@Override
	public Actor[] getActors() {
		return (Actor[]) actors.toArray();
	}

	public int[] coordinates() {
		return new int[] { x, y };
	}

	@Override
	public boolean equals(Object that) {
		return that == this
				|| that instanceof DefaultLocation
				&& ((DefaultLocation) that).coordinates().equals(
						this.coordinates());
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public String toString() {

		return "DefaultLocationAt("+x+","+y+")";
	}

	@Override
	public Set<Location> getNeighborsSet() {
		return neighbors;
	}

}
