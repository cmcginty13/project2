package gordon.colin.mobile.adventure;

public class Player extends AbstractActor{

	@Override
	public boolean moveTo(Location location) {
		if (currentLocation.getNeighborsSet().contains(location)){
			currentLocation.actorExit(this);
			currentLocation = location;
			currentLocation.actorEnter(this);
			return true;
		}	else{
			return false;
		}
		
	}

	
}
