package gordon.colin.mobile.adventure;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

public class DrawTest extends SurfaceView implements SurfaceHolder.Callback, OnTouchListener{

	private final String TAG = "DrawTest";
	SurfaceHolder surHolder;
	DrawingThread drawThread;
	Location terrain;
	
	public DrawTest(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		surHolder = getHolder();
		surHolder.addCallback(this);
		drawThread = new DrawingThread(surHolder, this, terrain);
		setOnTouchListener(this);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		drawThread.start();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public boolean onTouch(View v, MotionEvent event) {
		drawThread.interrupt();
		drawThread = new DrawingThread(surHolder, this, terrain, event);
		drawThread.start();
		return false;
	}

	

}
